## Isomap presentation notes

### Intro
- Itroduce the paper and ideas in it.

### Basics
- Isomap is essentially MDS with (an approximation of) geodesic distance as the distance metric

- The approximation of the geodesic distance is computed as the graph distance in a neighborhood graph

#### Simple example
- We want to reduce the dimensionality of a "complex" 3-dimensional  dataset to 2 dimensions.
- We have a S-shaped data set in 3D:
    - points are colored by their position along the S-shaped curve.

- The three steps of the Isomap algorithms are as follows:
    1. Compute the pairwise distances (according to some appropriate metric), and construct a neighborhood graph by connecting the K-nearest neighbors (K-isomap) OR connecting points if they are withinh a radius $\epsilon$ from each other ($\epsilon$-isomap).
    The weights in the graph are the distances between the points.
    2. The approximation of the geodesic distance between the points are calculated as path distances in this graph. Algorithms like Dijkstra’s or Floyd-Warshall algorithm can be used to calculate the distances.
    3. The pairwise distances in the graph are used by the MDS algorithm to find an embedding in a d-dimensional Euclidean space that best preserves the datas instrinsic geometry.

