\documentclass{beamer}

\setbeamertemplate{itemize items}[circle]

%Import the natbib package and sets a bibliography  and citation styles
\usepackage{natbib}
\bibliographystyle{apalike}
\setcitestyle{authoryear}
 
\usepackage{float}
\usepackage{graphicx} % For showing images 
\graphicspath{ {images/} } % Image folder

\usepackage{tikz}
\usetikzlibrary{positioning}
 
%Information to be included in the title page:
\title{A Global Geometric Framework for Nonlinear Dimensionality Reduction}
\subtitle{Machine Learning and Algorithmics Seminar 2018}
\author{Fredrik Fagerholm}
\date{\today}
 
 
\begin{document}
 
\frame{\titlepage}
 
\begin{frame}
\frametitle{Introduction}
\begin{itemize}
\item Paper: "A Global Geometric Framework for Nonlinear Dimensionality 
Reduction" in \emph{Science}, Joshua B. Tenenbaum, Vin de Silva, John C. Langford. (2001) \\

\item Introduces the Isomap algorithm for dimensionality reduction.\\

\item Isomap is a \textbf{nonlinear} dimensionality reduction technique.

\item Compared to other classical approaces (PCA, MDS), Isomap is able
to find "better" low-dimensional representations of the data in some cases.

\end{itemize}
\end{frame}


\begin{frame}
\frametitle{Outline}
\begin{itemize}
\item Motivation
\item Intuition behind the method
\item Algorithm
\item Applications
\item Comparison to other methods
\end{itemize}
\end{frame}


\begin{frame}
\frametitle{Dimensionality reduction}
\begin{itemize}
\item Dimensionality reduction is the problem of finding a meaningful 
low-dimensional representation of high dimensional data. \\

\item "high-dimensional" data  means that it has a high
number of features compared to the number of samples ($d >> n$). \\

\item This is a common problem when working with data such as images, 
biological data (e.g. microarrays), climate data, recommender systems etc. \\

\item High dimensionality may lead to problems in data-analysis and machine learning:
\emph{curse of dimensionality} (data becomes sparse as the number of dimensions 
increase). \\
\end{itemize}
\end{frame}


\begin{frame}
\frametitle{Basic idea}

\begin{itemize}
\item Isomap is essentially multidimensional scaling with \emph{geodesic distance} 
as the distance metric.

\item The geodesic distance between points is approximated by the distance in a 
neighborhood graph.

\item Isomap is able to recover the structure of nonlinear manifolds
\footnote{A manifold is a topological space that is locally Euclidean.}

\end{itemize}

\begin{figure}[H]
  	\centering
		\includegraphics[width=\textwidth]{swissroll}
	\caption{Approximating the geodesic distance by the
	distance in a neighborhood graph.}
	\label{fig:swissroll}
\end{figure}

\end{frame}

\begin{frame}
\frametitle{Basic idea}
\begin{center}
  \begin{tikzpicture}
    \node<1> (img1) {
    	\includegraphics[width=\textwidth,height=\textheight,keepaspectratio]{scatter_points}
    };
    \node<2> (img2) {
      	\includegraphics[width=\textwidth,height=\textheight,keepaspectratio]{distance_graph}
    };
    \node<3> (img3) {
	  	\includegraphics[height=8cm,keepaspectratio]{isomap_embedding}
    };
    \node<4> (img4) {
	  	\includegraphics[height=8cm,keepaspectratio]{pca_embedding}
    };
  \end{tikzpicture}
\end{center}
\end{frame}

\begin{frame}
\frametitle{Isomap algorithm}
The isomap algorithm takes the pairwise distances as input, measured by the Euclidean metric (or some other domain-specific metric). Choose the number of neighbors K or a neighborhood radius $\epsilon$.
\begin{enumerate}
\item Construct the neighborhood graph. Add edges between points that are closer than $\epsilon$ ($\epsilon$-Isomap), or points in the set of K-nearest neighbors (K-Isomap). The weight of each edge is the distance between the points.

\item Compute shortest paths in the graph. The approximation of the pairwise geodesic distances are computed as the shortest paths in the graph\footnote{Dijkstra’s or Floyd-Warshall algorithm can be used}, and stored in a matrix $D_G$.

\item Contruct embedding. Find the eigenvalues $\lambda_k$ (in decreasing order) and eigenvectors $v_k$ of $\tau(D_G)$\footnote{$\tau(D) = -1/2 \cdot HSH$, were $S_{ij} = D_{ij}^2$ and $H = \mathrm{I} - 1/n \cdot \mathbf{1}\mathbf{1}^\mathsf{T}$}. The entries of the k:th coordinate vector $y_k$ is calculated as $y_k[i] = \sqrt{\lambda_k}v_k[i]$\footnote{classic multidimensional scaling}.
\end{enumerate}
\end{frame}

\begin{frame}
\frametitle{Synthetic example}
\begin{itemize}
\item A data set consisting of 698 images of a face viewed under different pose and lighting conditions.
		
\item The images lie on a intrinsically three-dimensional manifold: parameterized 
by two pose variables plus an lighting angle.
		
\item 64 by 64 pixel images, represented as 4096-dimensional vectors.
		
\item Can we find a lower dimensional (3D) space that captures the meaningful
variations in the data?
	\end{itemize}
\end{frame}

\begin{frame}
\begin{figure}[H]
  	\centering
		\includegraphics[width=\textwidth,height=7cm,keepaspectratio]{faces}
	\caption{Three-dimensional embedding of the data, 
	learned by the Isomap algorithm with $K = 6$. The third dimension (lighting direction)
	is fixed.}
	\label{fig:faces}
\end{figure}
\end{frame}

\begin{frame}
\frametitle{Real-world example}
\begin{itemize}
\item 1000 images of handwritten "2"s from the MNIST data set. 
		
\item 28 by 28 pixel images, as 784-dimensional vectors.
		
\item $\epsilon$-Isomap is used, with $\epsilon = 4.2$, and tanget distance 
is used as the distance metric.
		
\item The intrinsic dimensionality is not known, but Isomap finds a "intuitive" 
low-dimensional representation. 
\end{itemize}
\end{frame}

\begin{frame}
\begin{figure}[H]
  	\centering
		\includegraphics[width=\textwidth,height=7cm,keepaspectratio]{numbers}
	\caption{Two most significant dimensions in the Isomap embedding.}
	\label{fig:faces}
\end{figure}
\end{frame}


\begin{frame}
\frametitle{Comparison with other methods}

\begin{itemize}
\item PCA finds the embedding that best preserves the variance measured in the input space.

\item MDS preserves the interpoint distances measured in the input space as well as possible.

\item Isomap preserves the distance on nonlinear structures measured in the input space.

\item The methods can be compared by plotting the residual varaince\footnote{$1 - R(\hat{D_M} , D_Y)$, where $D_Y$ is the matrix of Euclidean distances in the low-dimensional embedding recovered by each algorithm. $D_M$ is each algorithm’s best estimate of the intrinsic manifold distances. R is the standard linear correlation coefficient} for projections of the data
on subspaces of different dimension. 

\end{itemize}
\end{frame}

\begin{frame}
\begin{figure}[H]
  	\centering
\begin{minipage}[b]{0.45\linewidth}
	\centering
    \includegraphics[width=\textwidth]{A}
\end{minipage}
\hspace{0.5cm}
\begin{minipage}[b]{0.45\linewidth}
	\centering
	\includegraphics[width=\textwidth]{D}
\end{minipage}
\caption{Residual variance for Isomap (filled circles), PCA (open triangles) and MDS (open circles). (A) Face images, (D) MNIST "2"s. The elbow of the graph (where the curve ceases to decrease significantly) indicates the instrinsic dimensionality. The x-axis shows the number of dimensions in the embedding, y-axis shows the residual variance.}
\label{fig:residual_variance}
\end{figure}
\end{frame}

\begin{frame}
\frametitle{References}
\nocite{*}
\bibliography{references}
\end{frame}


 
\end{document}