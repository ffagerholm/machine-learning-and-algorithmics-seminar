## Isomap
Presentation of Isomap algorithm for course Machine Learning and Algorithmics Seminar 2018 at University of Turku.

### Presentation
Latex source for beamer presentation can be found in directory `presentation`.

### Notebook
An example of how isomap embedding works can be found in the jupyter notebook `isomap_example.ipynb`. 
